#!/usr/bin/perl

use File::Basename;
use File::Temp;
use File::Spec::Functions;
use Sys::Hostname;

sub prompt {
    my ($prompt, $default) = @_;
    print("$prompt [$default] : ");
    chop ($input=<STDIN>);
    if ($input eq "") {
        defined($default) or die "ERROR: $prompt must be specified\n";
        $input=$default;
    }
    return $input;
}

sub sanitizedForFilename {
    my ($text) = @_;
    $text =~ s/[^a-zA-Z0-9\-_\.]/_/g;
    return $text;
}

$args = join " ", @ARGV;

$TESTPLATFORM = prompt("Test platform", $ENV{QTUITEST_PLATFORM});
@platforms = ("symbian", "linux", "maemo", "meego", "windows", "wince", "mac");
for (@platforms) {
    if (index ($TESTPLATFORM, $_) != -1) { goto platform_ok; }
}
die "ERROR: Test platform must contain one of: @platforms\n";
platform_ok:

$TESTCHANGE = prompt("Change or build number", $ENV{QTUITEST_CHANGE});
$TESTBRANCH = prompt("Branch", $ENV{QTUITEST_BRANCH});
$TESTTESTR = prompt("Upload location", $ENV{QTUITEST_TESTR});

$TIMESTAMP=time();
$HOSTNAME=hostname;
if (defined $ENV{BUILDDIR}) {
    $TESTNAME=basename($ENV{BUILDDIR});
    chomp $TESTNAME;
    chomp $TESTNAME;
} else {
    $TESTNAME = "results";
}
$TEMPDIR=($ENV{TMP} or $ENV{TMPDIR});

$RESULTSDIR=catfile($TEMPDIR, "qtuitest_results_$TIMESTAMP");
$RESULTSFILE=catfile($RESULTSDIR, "$TESTNAME.xml");

($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = gmtime($TIMESTAMP);
$datefmt = sprintf "%04u-%02u-%02u", $year+1900, $mon+1, $mday;
$timefmt = sprintf "%02u:%02u:%02u", $hour, $min, $sec;

$UPLOADFILENAME=sanitizedForFilename("result_qtuitest_WITH_$TESTPLATFORM" . "_AT_$HOSTNAME" . "_ON_$datefmt" . "_$timefmt" . "_USING_$TESTBRANCH.xml");
$UPLOADFILE=catfile($RESULTSDIR, $UPLOADFILENAME);

mkdir $RESULTSDIR;
print("Results directory is $RESULTSDIR\n");

print "qtuitestrunner $args -xml -o $RESULTSFILE\n";
system "qtuitestrunner $args -xml -o $RESULTSFILE";
-e $RESULTSFILE or die "ERROR: Results file $RESULTSFILE does not exist!";
print "upload file is $UPLOADFILE\n";

$header = <<EOF;
<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
<Testrun>
<Environment>
  <HostName>$HOSTNAME</HostName>
  <MakeSpec>$TESTPLATFORM</MakeSpec>
  <ChangeNumber>$TESTCHANGE</ChangeNumber>
  <Branch>$TESTBRANCH</Branch>
</Environment>
<Timestamp date=\"$datefmt\" time=\"$timefmt\"/>
EOF
$footer = "</Testrun>\n";

open XMLOUT, ">$UPLOADFILE";
print XMLOUT $header;
open XMLIN, "<$RESULTSFILE";
@results = <XMLIN>;
shift @results;
print XMLOUT "\n<!-- file 1 of 1: $RESULTSFILE -->\n";
print XMLOUT @results;
print XMLOUT $footer;
close XMLIN;
close XMLOUT;

# By using chdir we avoid having to mangle the upload file path
# on Windows based on whether msys scp is used.
chdir $RESULTSDIR;

if (defined $ENV{QTUITEST_SCP}) {
    $SCP=$ENV{QTUITEST_SCP};
} else {
    $SCP = "scp";
}
if (system $SCP, $UPLOADFILENAME, $TESTTESTR) {
    die "ERROR: Failed to send file using $SCP.\nPlease check you have entered the correct details.\nThe upload file is: $UPLOADFILE\n";
}
