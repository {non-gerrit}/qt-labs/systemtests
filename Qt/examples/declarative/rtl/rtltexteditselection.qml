/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1

Flickable {
    height: 640; width: 360; contentHeight: root.height; contentWidth: 360; boundsBehavior: Flickable.StopAtBounds
    Rectangle {
        id: root
        width: 300; height: 600; anchors.centerIn: parent
        color: "white"
        property string text: "مرحبا العالم"
        property string pastetext
        property variant horizontalAlignment: undefined
        Column {
            id: containerColumn
            spacing: 10
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: 5
            TextEdit {
                id: tedit
                wrapMode: TextEdit.Wrap
                width: parent.width
                height: 250
                text: root.text
                font.pixelSize: 30
                horizontalAlignment: root.horizontalAlignment
                selectByMouse: true
                Rectangle {
                    z: -1
                    anchors.fill: parent
                    color: Qt.rgba(0.5, 0.5, 0.2, 0.3)
                }
                onSelectedTextChanged: { if(selectedText != ""){ copy(); root.pastetext = selectedText; } }
                Text {
                    color: "white"
                    text: {
                        if (tedit.horizontalAlignment == undefined)
                            return "I";
                        switch (tedit.horizontalAlignment) {
                        case Text.AlignLeft:
                            return "L";
                        case Text.AlignRight:
                            return "R";
                        case Text.AlignHCenter:
                            return "C";
                        case Text.AlignJustify:
                            return "J";
                        }
                    }
                    anchors{ top: parent.top; right: parent.right; margins: 2 }
                }
            }
            Rectangle {
                height: 50; width: parent.width; color: Qt.rgba(0.5, 0.5, 0.2, 0.3); radius: 10; border.color: "gray"
                MouseArea { anchors.fill: parent; onClicked: { tedit.paste(); } }
                Text {
                    height: 25; width: parent.width - 10; anchors.centerIn: parent; font.pixelSize: 20
                    text: "Paste: " + root.pastetext
                }
            }
            Rectangle {
                height: 50; width: parent.width; color: Qt.rgba(0.5, 0.5, 0.2, 0.3); radius: 10; border.color: "gray"
                MouseArea { anchors.fill: parent
                    property int index: 0
                    onClicked: {
                        if (index < 0) {
                            root.horizontalAlignment = undefined;
                        } else {
                            root.horizontalAlignment = Math.pow(2, index);
                        }
                        index = (index + 2) % 5 - 1;
                    }
                }
                Text {
                    height: 25; width: parent.width - 10; anchors.centerIn: parent; font.pixelSize: 20
                    text: {
                        if (root.horizontalAlignment == undefined)
                            return "Implicit alignment";
                        switch (tedit.horizontalAlignment) {
                        case Text.AlignLeft:
                            return "Left alignment";
                        case Text.AlignRight:
                            return "Right alignment";
                        case Text.AlignHCenter:
                            return "Center alignment";
                        case Text.AlignJustify:
                            return "Justify alignment";
                        }
                    }
                }
            }
        }
    }
}