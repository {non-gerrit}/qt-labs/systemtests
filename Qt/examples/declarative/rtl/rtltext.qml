/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1

Flickable {
    height: 640; width: 360; contentHeight: root.height; contentWidth: 360; boundsBehavior: Flickable.StopAtBounds
    Rectangle {
        id: root
        width: 240; height: containerColumn.height+containerColumn.anchors.topMargin; anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        property variant text: ["", " ", "Hello world!", "مرحبا العالم"]
        property variant description: ["empty text", "white-space-only text", "left-to-right text", "right-to-left text"]
        property variant editorType: ["Text", "TextInput", "TextEdit"]
        property variant horizontalAlignment: undefined
        Column {
            id: containerColumn
            spacing: 10
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: 5
            Column {
                id: editorTypeColumn
                spacing: 5
                width: parent.width
                Repeater {
                    model: root.resources.length-1
                    Item {
                        width: yetAnotherColumn.width
                        height: yetAnotherColumn.height
                        anchors.horizontalCenter: parent.horizontalCenter
                        Column {
                            id: yetAnotherColumn
                            width: textColumn.width+20
                            spacing: 5
                            Text {
                                text: root.editorType[index]
                                font.pixelSize: 20
                                anchors.horizontalCenter: parent.horizontalCenter
                            }
                            Column {
                                id: textColumn
                                spacing: 10
                                anchors.horizontalCenter: parent.horizontalCenter
                                Repeater {
                                    model: root.text.length
                                    delegate: root.resources[index+1]
                                }
                            }
                        }
                    }
                }
            }

            Text {
                text: "Default application\ndirection is " + (Qt.application.layoutDirection == Qt.RightToLeft ? "Right-to-left" : "Left-to-right")
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 15
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Rectangle {
                id: button
                height: 50; width: parent.width
                color: mouseArea.pressed ? "black" : "gray"
                Text {
                    text: {
                        if (root.horizontalAlignment == undefined)
                            return "Implicit alignment";
                        switch (root.horizontalAlignment) {
                        case Text.AlignLeft:
                            return "Left alignment";
                        case Text.AlignRight:
                            return "Right alignment";
                        case Text.AlignHCenter:
                            return "Center alignment";
                        case Text.AlignJustify:
                            return "Justify alignment";
                        }
                    }
                    color: "white"
                    font.pixelSize: 16
                    anchors.centerIn: parent
                }
                MouseArea {
                    id: mouseArea
                    property int index: 0
                    onClicked: {
                        if (index < 0) {
                            root.horizontalAlignment = undefined;
                        } else {
                            root.horizontalAlignment = Math.pow(2, index);
                        }
                        index = (index + 2) % 5 - 1;
                    }
                    anchors.fill: parent
                }
            }
        }
        Component {
            id: textComponent
            Text {
                width: 200
                text: root.text[index]
                font.pixelSize: 30
                horizontalAlignment: root.horizontalAlignment
                Rectangle {
                    z: -1
                    anchors.fill: parent
                    color: Qt.rgba(0.8, 0.2, 0.2, 0.3)
                }
                Text {
                    text: root.description[index]
                    color: Qt.rgba(1,1,1,1.0)
                    anchors.centerIn: parent
                    Rectangle {
                        z: -1
                        anchors { fill: parent; margins: -3 }
                        color: Qt.rgba(0.3, 0, 0, 0.3)
                    }
                }
                Text {
                    color: "white"
                    text: {
                        if (parent.horizontalAlignment == undefined)
                            return "I";
                        switch (parent.horizontalAlignment) {
                        case Text.AlignLeft:
                            return "L";
                        case Text.AlignRight:
                            return "R";
                        case Text.AlignHCenter:
                            return "C";
                        case Text.AlignJustify:
                            return "J";
                        }
                    }
                    anchors{ top: parent.top; right: parent.right; margins: 2 }
                }
            }
        }
        Component {
            id: textInputComponent
            TextInput {
                width: 200
                text: root.text[index]
                font.pixelSize: 30
                horizontalAlignment: root.horizontalAlignment
                Rectangle {
                    z: -1
                    anchors.fill: parent
                    color: Qt.rgba(0.6, 0.4, 0.2, 0.3)
                }
                Text {
                    text: root.description[index]
                    color: Qt.rgba(1,1,1,1.0)
                    anchors.centerIn: parent
                    Rectangle {
                        z: -1
                        anchors { fill: parent; margins: -3 }
                        color: Qt.rgba(0.3, 0, 0, 0.3)
                    }
                }
                Text {
                    color: "white"
                    text: {
                        if (parent.horizontalAlignment == undefined)
                            return "I";
                        switch (parent.horizontalAlignment) {
                        case Text.AlignLeft:
                            return "L";
                        case Text.AlignRight:
                            return "R";
                        case Text.AlignHCenter:
                            return "C";
                        case Text.AlignJustify:
                            return "J";
                        }
                    }
                    anchors{ top: parent.top; right: parent.right; margins: 2 }
                }
            }
        }
        Component {
            id: textEditComponent
            TextEdit {
                width: 200
                text: root.text[index]
                font.pixelSize: 30
                horizontalAlignment: root.horizontalAlignment
                Rectangle {
                    z: -1
                    anchors.fill: parent
                    color: Qt.rgba(0.5, 0.5, 0.2, 0.3)
                }
                onSelectedTextChanged: { copy(); }
                selectByMouse: true
                Text {
                    text: root.description[index]
                    color: Qt.rgba(1,1,1,1.0)
                    anchors.centerIn: parent
                    Rectangle {
                        z: -1
                        anchors { fill: parent; margins: -3 }
                        color: Qt.rgba(0.3, 0, 0, 0.3)
                    }
                }
                Text {
                    color: "white"
                    text: {
                        if (parent.horizontalAlignment == undefined)
                            return "I";
                        switch (parent.horizontalAlignment) {
                        case Text.AlignLeft:
                            return "L";
                        case Text.AlignRight:
                            return "R";
                        case Text.AlignHCenter:
                            return "C";
                        case Text.AlignJustify:
                            return "J";
                        }
                    }
                    anchors{ top: parent.top; right: parent.right; margins: 2 }
                }
            }
        }
    }
}