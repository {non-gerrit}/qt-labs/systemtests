/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1

Rectangle {
    width: 640
    height: 360
    color: pinchy.pinch.active ? "blue" : "gray"
    Rectangle { color: "transparent"; border.color: "black"; x: 150; y: 150; height: 250; width: 250 }

    PinchArea {
        id: pinchy
        pinch.target: logo
        enabled: false
        anchors.fill: parent
        pinch.dragAxis: Pinch.XandYAxis
        pinch.minimumScale: 0.5
        pinch.maximumScale: 2
        pinch.minimumX: 50
        pinch.maximumX: 300
        pinch.minimumY: 50
        pinch.maximumY: 300
        pinch.minimumRotation: -150
        pinch.maximumRotation: 150
    }

    Rectangle {
        id: logo; width: 200; height: 200; x: 100; y: 100; color: "white"
        Image {
            anchors.fill: parent
            source: "qt-logo.jpg"
        }
    }

    Rectangle {
        id: enabler; height: 100; width: 100; anchors.right: parent.right; color: pinchy.enabled ? "green" : "red"
        Text {
            anchors.centerIn: parent; text: pinchy.enabled ? "Disable" : "Enable"
        }
        MouseArea {
            anchors.fill: parent; onClicked: pinchy.enabled = !pinchy.enabled
        }
    }
    Item {
        Column {
            Text {
                text: "Scale: " + pinchy.pinch.target.scale + " (" + pinchy.pinch.minimumScale + "/" + pinchy.pinch.maximumScale + ")"
            }
            Text {
                text: "Coord: " + pinchy.pinch.target.x + "/" + pinchy.pinch.target.y + " (" + pinchy.pinch.minimumX + "/" + pinchy.pinch.maximumX + "," + pinchy.pinch.minimumY + "/" + pinchy.pinch.maximumY + ")"
            }
            Text {
                text: "Rotation: " + pinchy.pinch.target.rotation + " (" + pinchy.pinch.minimumRotation + "/" + pinchy.pinch.maximumRotation + ")"
            }
        }
    }
    Rectangle {
        id: exit; height: 100; width: 100; anchors.right: parent.right; anchors.bottom: parent.bottom; color: "red"
        Text { anchors.centerIn: parent; text: "Exit" }
        MouseArea { anchors.fill: parent; onClicked: { Qt.quit(); } }
    }
    Rectangle {
        id: reload; height: 100; width: 100; anchors.right: parent.right; anchors.verticalCenter: parent.verticalCenter; color: "green"
        Text { anchors.centerIn: parent; text: "Reload" }
        MouseArea { anchors.fill: parent; onClicked: { qmlViewer.reload(); } }
    }

    Item {
        height: runtime.orientation != 2 ? parent.height : 0
        Behavior on height { NumberAnimation { duration: 200 } }
        width: runtime.orientation != 2 ? parent.width : 0
        Behavior on width { NumberAnimation { duration: 200 } }
        anchors.centerIn: parent
        Rectangle {
            height: parent.height - 15; width: parent.width - 15; color: "green"; radius: 10; anchors.centerIn: parent; border.color: "blue"
            Text { visible: runtime.orientation != 2; width: parent.width - 30; anchors.centerIn: parent; horizontalAlignment: Text.AlignHCenter
                text: "Please change to landscape orientation\nvia the Settings menu, or\nrotating the device"
            }
        }
        MouseArea { anchors.fill: parent }
    }
}
