/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of QtUiTest.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1

Rectangle {
    height: 360; width: 640; color: "gray"

    Rectangle { id: textinput1box; width: 200; height: 30; color: "green"; border.color: textinput1.activeFocus ? "red" : "green"
        anchors.left: parent.left; anchors.leftMargin: 20; anchors.top: parent.top; anchors.topMargin: 10 }

    Rectangle {
        id: textinput2box; width: 200; height: 30; color: "lightblue"; border.color: textinput2.activeFocus ? "red" : "lightblue"
        anchors.left: parent.left; anchors.leftMargin: 20; anchors.top: textinput1.bottom; anchors.topMargin: 10 }

    TextInput {
        id: textinput1
        anchors.fill: textinput1box
        font.bold: bolded.active
        font.italic: italics.active
        font.strikeout : strikeouton.active
        font.underline : underlineon.active
        smooth: smoothon.active
        cursorVisible: cursoron.active
        autoScroll: autoscrollon.active
        focus: true
        onFocusChanged: { if(focus){ cursoron.active = true; } }
    }

    Column {
        anchors.left: textinput1.right; anchors.leftMargin: 5; spacing: 2
        Text {
            id: textinputinfo
            text: "Pos: "+ textinput1.cursorPosition +" Sel: "+ textinput1.selectionStart +"-"+ textinput1.selectionEnd +" Max: "+ textinput1.maximumLength
        }

        Row {
            id: textcolor
            Text { text: "Text Color: " }
            Rectangle { color: "black"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.color = "black" } } }
            Rectangle { color: "red"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.color = "red" } } }
            Rectangle { color: "brown"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.color = "brown" } } }
        }
        Row {
            id: selectedcolor
            Text { text: "Selected Text Color: " }
            Rectangle { color: "black"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.selectedTextColor = "black" } } }
            Rectangle { color: "red"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.selectedTextColor = "red" } } }
            Rectangle { color: "brown"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.selectedTextColor = "brown" } } }
        }
        Row {
            id: selectioncolor
            Text { text: "Selection Color: " }
            Rectangle { color: "black"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.selectionColor = "black" } } }
            Rectangle { color: "red"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.selectionColor = "red" } } }
            Rectangle { color: "brown"; height: 20; width: 20; MouseArea { anchors.fill: parent; onClicked: { textinput1.selectionColor = "brown" } } }
        }
    }

    TextInput {
        id: textinput2
        anchors.fill: textinput2box
    }

    Rectangle { anchors.fill: controlsview; color: "transparent"; border.color: "black" }
    ListView {
        id: controlsview
        model: controls
        height: parent.height *.6; width: parent.width - 10; orientation: ListView.Horizontal
        anchors.horizontalCenter: parent.horizontalCenter; anchors.bottom: parent.bottom; anchors.bottomMargin: 5
    }

    VisualItemModel {
        id: controls
        // Cut, copy and paste
        Item {
            id: cutcopypaste
            height: controlsview.height; width: 100
            Column {
                width: parent.width - 4; height: parent.height - 4; anchors.centerIn: parent
                Rectangle {
                    id: copybutton
                    height: parent.height/3; width: parent.width; color: "gray"; border.color: "black"; radius: 5
                    Text { anchors.centerIn: parent; text: "Copy" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.copy(); } }
                }
                Rectangle {
                    id: cutbutton
                    height: parent.height/3; width: parent.width; color: "gray"; border.color: "black"; radius: 5
                    Text { anchors.centerIn: parent; text: "Cut" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.cut(); } }
                }
                Rectangle {
                    id: pastebutton
                    height: parent.height/3; width: parent.width; color: "gray"; border.color: "black"; radius: 5
                    Text { anchors.centerIn: parent; text: "Paste" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput2.paste(); } }
                }
            }
        }

        // Text selection
        Item {
            id: selection
            height: controlsview.height; width: 100
            Column {
                width: parent.width - 4; height: parent.height - 4; anchors.centerIn: parent
                Rectangle {
                    id: selectallbutton
                    height: parent.height/4; width: parent.width; color: "gray"; border.color: "black"; radius: 5
                    Text { anchors.centerIn: parent; text: "Select All" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.selectAll(); } }
                }
                Rectangle {
                    id: selectwordbutton
                    height: parent.height/4; width: parent.width; color: "gray"; border.color: "black"; radius: 5
                    Text { anchors.centerIn: parent; text: "Select Word" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.selectWord(); } }
                }
                Rectangle {
                    id: selectnonebutton
                    height: parent.height/4; width: parent.width; color: "gray"; border.color: "black"; radius: 5
                    Text { anchors.centerIn: parent; text: "Select None" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.deselect(); } }
                }
                Rectangle {
                    id: mouseselecttoggle
                    height: parent.height/4; width: parent.width; color: textinput1.selectByMouse ? "green" : "gray"; border.color: "black"; radius: 5
                    Text { anchors.centerIn: parent; text: textinput1.selectByMouse ? "Mouse On" : "Mouse Off" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.selectByMouse = !textinput1.selectByMouse; } }
                }
            }
        }

        // Echo methods
        Item {
            id: echostyle
            height: controlsview.height; width: 100
            Column {
                width: parent.width - 4; height: parent.height - 4; anchors.centerIn: parent
                Rectangle {
                    id: echonormalbutton
                    height: parent.height/4; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.echoMode == TextInput.Normal ? "green" : "gray";
                    Text { anchors.centerIn: parent; text: "Normal" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.echoMode = TextInput.Normal; } }
                }
                Rectangle {
                    id: echopasswordbutton
                    height: parent.height/4; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.echoMode == TextInput.Password ? "green" : "gray";
                    Text { anchors.centerIn: parent; text: "Password" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.echoMode = TextInput.Password; } }
                }
                Rectangle {
                    id: noechobutton
                    height: parent.height/4; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.echoMode == TextInput.NoEcho ? "green" : "gray";
                    Text { anchors.centerIn: parent; text: "No Echo" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.echoMode = TextInput.NoEcho; } }
                }
                Rectangle {
                    id: echooneditbutton
                    height: parent.height/4; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.echoMode == TextInput.PasswordEchoOnEdit ? "green" : "gray";
                    Text { anchors.centerIn: parent; text: "Edit Password" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.echoMode = TextInput.PasswordEchoOnEdit; } }
                }
            }
        }

        // Text Styling
        Item {
            id: styling
            height: controlsview.height; width: 200
            Row {
                Column {
                    id: buttonlist
                    ToggleButton { id: bolded; effectname: "Bold" }
                    ToggleButton { id: italics; effectname: "Italics" }
                    ToggleButton { id: strikeouton; effectname: "Strikeout" }
                    ToggleButton { id: underlineon; effectname: "Underline" }
                }
                Column {
                    ToggleButton { id: smoothon; effectname: "Smooth" }
                    ToggleButton { id: cursoron; effectname: "Cursor"; active: true }
                    ToggleButton { id: autoscrollon; effectname: "Autoscroll" }
                }
            }
        }

        // Capitalization - MixedCase, AllUppercase, AllLowercase, SmallCaps, Capitalize
        Item {
            id: capitals
            height: controlsview.height; width: 100
            Column {
                width: parent.width - 4; height: parent.height - 4; anchors.centerIn: parent
                Rectangle {
                    id: capsmixedbutton
                    height: parent.height/5; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.font.capitalization == Font.MixedCase ? "green" : "gray";
                    Text { anchors.centerIn: parent; text: "Mixed" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.font.capitalization = Font.MixedCase; } }
                }
                Rectangle {
                    id: capsallupperbutton
                    height: parent.height/5; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.font.capitalization == Font.AllUppercase ? "green" : "gray"
                    Text { anchors.centerIn: parent; text: "All Upper" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.font.capitalization = Font.AllUppercase; } }
                }
                Rectangle {
                    id: capsalllowerbutton
                    height: parent.height/5; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.font.capitalization == Font.AllLowercase ? "green" : "gray"
                    Text { anchors.centerIn: parent; text: "All lower" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.font.capitalization = Font.AllLowercase; } }
                }
                Rectangle {
                    id: capsmallbutton
                    height: parent.height/5; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.font.capitalization == Font.SmallCaps ? "green" : "gray"
                    Text { anchors.centerIn: parent; text: "Small caps" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.font.capitalization = Font.SmallCaps; } }
                }
                Rectangle {
                    id: capcapitilizebutton
                    height: parent.height/5; width: parent.width; border.color: "black"; radius: 5
                    color: textinput1.font.capitalization == Font.Capitalize ? "green" : "gray"
                    Text { anchors.centerIn: parent; text: "Capitalize" }
                    MouseArea { anchors.fill: parent; onClicked: { textinput1.font.capitalization = Font.Capitalize; } }
                }
            }
        }
    }

    Item {
        height: runtime.orientation != 2 ? parent.height : 0
        Behavior on height { NumberAnimation { duration: 200 } }
        width: runtime.orientation != 2 ? parent.width : 0
        Behavior on width { NumberAnimation { duration: 200 } }
        anchors.centerIn: parent
        Rectangle {
            height: parent.height - 15; width: parent.width - 15; color: "green"; radius: 10; anchors.centerIn: parent; border.color: "blue"
            Text { visible: runtime.orientation != 2; width: parent.width - 30; anchors.centerIn: parent; horizontalAlignment: Text.AlignHCenter
                text: "Please change to landscape orientation\nvia the Settings menu, or\nrotating the device"
            }
        }
        MouseArea { anchors.fill: parent }
    }
}