TEMPLATE=subdirs
SUBDIRS+= \
    sys_bearer \
    sys_calendar \
    sys_contacts \
    sys_crsysteminfo \
    sys_declarative-music-browser \
    sys_declarative-sfw-notes \
    sys_location \
    sys_messaging \
    sys_multimedia \
    sys_oopserviceframework \
    sys_sensors \
    sys_tactilefeedback \
    sys_telephony \
    sys_versit \
    sys_versitorganizer \

