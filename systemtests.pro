TEMPLATE=subdirs
SUBDIRS=ASFOrbit Qt QtMobility
#CONFIG+=ordered

system(echo DEFINES+=QTUITEST_IMAGE_PATH=$$OUT_PWD > $$OUT_PWD/.qmake.cache)
system(echo QMAKEFEATURES+=$$PWD/features >> $$OUT_PWD/.qmake.cache)

unix:!mac:!symbian {
    system(if ! [ -e $$OUT_PWD/features ]; then ln -sf $$PWD/features $$OUT_PWD/features; fi)
    system(echo SRCROOT=$(readlink -f $$PWD) >> $$OUT_PWD/.qmake.cache)
    system(echo BUILDROOT=$(readlink -f $$OUT_PWD) >> $$OUT_PWD/.qmake.cache)
}

mac {
    system(if ! [ -e $$OUT_PWD/features ]; then ln -sf $$PWD/features $$OUT_PWD/features; fi)
    system(echo SRCROOT='$$PWD' >> $$OUT_PWD/.qmake.cache)
    system(echo BUILDROOT='$$OUT_PWD' >> $$OUT_PWD/.qmake.cache)
}


symbian {
    SUBDIRS-=qtbindings libqsystemtest interpreter
    system(echo SRCROOT=$$PWD >> $$OUT_PWD/.qmake.cache)
    system(echo BUILDROOT=$$OUT_PWD >> $$OUT_PWD/.qmake.cache)
}

win32 {
    WIN_PWD=$$replace(PWD,/,\)
    WIN_OUT_PWD=$$replace(OUT_PWD,/,\)
    system(xcopy $$WIN_PWD\features $$WIN_OUT_PWD\features /E /I /D /Y)
    system(echo SRCROOT=$$PWD >> $$OUT_PWD/.qmake.cache)
    system(echo BUILDROOT=$$OUT_PWD >> $$OUT_PWD/.qmake.cache)
    system(echo INSTALLROOT=$$OUT_PWD/bin >> $$OUT_PWD/.qmake.cache)
}
